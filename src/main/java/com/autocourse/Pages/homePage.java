package com.autocourse.Pages;

import cucumber.api.java.en.Given;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;


public class homePage {

    AndroidDriver m_driver;

    //Locators
    protected static By TELEFONO = By.xpath("//android.widget.TextView[@text='Phone']");

    protected static By MENSAJES = null;
    protected static By SETTINGS = null;

    protected static By CHROME = By.xpath("//android.widget.TextView[@text='Chrome']");

    public homePage(AndroidDriver m_driver){

        this.m_driver = m_driver;
    }

    //methods


    public void clickChrome(){
        m_driver.findElement(CHROME).click();
    }



}
