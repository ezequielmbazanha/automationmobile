package autocourse.Tests;
import com.autocourse.Pages.homePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class steps {

    AndroidDriver m_driver;

    public void start() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        String udid = "ZL5229KQ3D";//
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, udid);
        capabilities.setCapability(MobileCapabilityType.UDID, udid);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "5.1");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
        //capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.launcher");
        //capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.android.launcher2.Launcher");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.motorola.launcher3");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.android.a1launcher.AndroidOneLauncher");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);

        URL remoteUrl = new URL("http://localhost:4723/wd/hub");
        m_driver = new AndroidDriver(remoteUrl, capabilities);
    }

    @Given("^open chrome")
    public void openChrome() throws Throwable {
        start();
        homePage hp = new homePage(m_driver);
        hp.clickChrome();

    }
}
